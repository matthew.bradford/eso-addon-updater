# Matthew's ESO Addon Updater

ESO's addon manger minion only works on Windows. Tired of manually unzipping and copying a bunch of files between folders? Here is something to help.

I worked on this enough that it does what I want, but if anyone else would like to use it I can put more effort into making it useful to other people. Let me know!

Running once will generate a eso_addon_config.ron file in executing directory.

Edit that file and set the addon directory.

Get url's for the addons you want to run this for from eso addon website.

## Example:


### eso_addon_config.ron:

(
    eso_addon_directory: "/home/******/.local/share/Steam/steamapps/compatdata/306130/pfx/drive_c/users/steamuser/My Documents/Elder Scrolls Online/live/AddOns/",
    addon_urls: [
        ("AUI", "https://www.esoui.com/downloads/download919-AUI-AdvancedUI"),
        ("Awesome Guild Store", "https://www.esoui.com/downloads/download695-AwesomeGuildStore"),
        ("Combat Alerts", "https://www.esoui.com/downloads/download1855-CodesCombatAlerts"),
        ("Combat Metrics", "https://www.esoui.com/downloads/download1360-CombatMetrics"),        
        ("Minimap", "https://www.esoui.com/downloads/download1399-VotansMinimap"),
    ],
)