use std::fs::File;
use std::io::Write;

pub fn open_file(file_name: &str) -> Option<File> {
    // Open config if found. Return None if not.
    match File::open(file_name) {
        Ok(f) => {
            println!("Config: {} File opened.", file_name);
            Some(f)
        }
        Err(e) => {
            eprintln!("Error opening {}: {:?}", file_name, e);
            None
        }
    }
}

pub fn write_file(file_name: &str, content: &[u8]) {
    match File::create(file_name) {
        Ok(mut f) => match f.write(content) {
            Ok(_) => {}
            Err(e) => {
                eprintln!("Erorr writing file: {}, {:?}", file_name, e);
            }
        },
        Err(e) => {
            eprintln!("Erorr creating file: {}, {:?}", file_name, e);
        }
    }
}