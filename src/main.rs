extern crate fs_extra;
extern crate ron;
extern crate reqwest;
#[macro_use]
extern crate serde;
extern crate zip;

use std::fs::{ DirBuilder, self };
use std::path::PathBuf;
use std::{
    io::{Result, self },
};

use fs_extra::dir::copy;

mod config;
mod file;
mod version;

pub const UNZIPPED: &str = "addons/unzipped";

fn main() {
    // Just create a couple folders in executing directory to store this stuff in.
    let path_addon_unzipped = PathBuf::from(UNZIPPED);
    prep_directory(&path_addon_unzipped).unwrap_or_else(|_| panic!("Failed to create unzipped directory: {:?}", path_addon_unzipped));

    let config = config::fetch();
    let mut vlist = version::fetch();

    let path_eso_addon_dir = config.get_eso_addon_dir().expect("Failed to get directory path from config. Check it is correctly formed.");
    let urls = config.get_addon_urls();
    for url in urls {
        let mut current_path = path_addon_unzipped.to_path_buf();
        current_path.push(&url.0);

        let update_url = latest_url(&url.1);
        if update_url.is_some() {
            let update_url = update_url.unwrap();
            let update_version = extract_file_name(&update_url);
            if update_version.is_some() {
                let update_version = update_version.unwrap();
                if vlist.outdated(&url.0, &update_version) {
                    // need to update addon
                    // 1. download zip
                    let dl = download_zip(&update_url).expect("Failed download.");
                    // 2. extract
                    unzip_download(&dl, &path_addon_unzipped).expect("Failed to unzip archive.");
                    // 3. copy
                    //copy_to_eso_addons(&unzipped_pb, &path_eso_addon_dir).expect("Failed copy from unzip to addon dir.");
                    // 4. update vlist version
                    vlist.update(&url.0, &update_version).expect("Failed to update version history list.");
                    version::save(&vlist).expect("Failed to save version history list.");
                }
            }
        }
    }
    copy_all_to_eso(&path_eso_addon_dir, &path_addon_unzipped).expect("Failed to copy files from unzipped to eso addons.");
}

fn copy_all_to_eso(eso_path: &PathBuf, unzip_path: &PathBuf) -> Result<()> {

    for dir in fs::read_dir(unzip_path).expect("Failed to read unzipped directory.") {
        let dir = dir.unwrap();
        if dir.path().is_dir() {
            match copy_to_eso_addons(&dir.path(), &eso_path) {
                Ok(_) => {},
                Err(e) => { eprintln!("Failed copy: {:?} Error: {:?}", dir, e); }
            }
        }
    }

    Ok(())
}

fn copy_to_eso_addons(source: &PathBuf, dest: &PathBuf) -> Result<()> {
    use fs_extra::dir::CopyOptions;
    let mut options = CopyOptions::new();
    options.overwrite = true;

    copy(&source, dest, &options).unwrap_or_else(|_| panic!("Failed to copy: {:?}", source));
    Ok(())
}

fn unzip_download(file: &[u8], dest: &PathBuf) -> Result<()> {
    let reader = std::io::Cursor::new(file);
    let mut zip = zip::ZipArchive::new(reader).expect("Failed to read zip");

    for  i in 0..zip.len() {
        let mut file = zip.by_index(i).unwrap();
        let mut outpath = dest.to_path_buf();
        outpath.push(file.sanitized_name());


        if (&*file.name()).ends_with('/') {
            fs::create_dir_all(&outpath).unwrap();
        } else {
            if let Some(p) = outpath.parent() {
                if !p.exists() {
                    fs::create_dir_all(&p).unwrap();
                }
            }
            let mut outfile = fs::File::create(&outpath).unwrap();
            io::copy(&mut file, &mut outfile).unwrap();
        }
    }

    Ok(())
}

fn latest_url(url: &str) -> Option<String> {
    let mut resp = reqwest::get(url).expect("request failed");

    let iden_start = "Problems with the download? <a href=\"";
    let iden_end = "\">Click here</a>.";

    let mut webdoc = resp.text().expect("Failed to get webdoc");

    let index_start = match webdoc.find(iden_start) {
        Some(x) => x,
        None => { return None; }
    };

    webdoc.replace_range(0..index_start + iden_start.len(), "");
    let index_end = match webdoc.find(iden_end) {
        Some(x) => x,
        None => { return None; }
    };

    webdoc.replace_range(index_end..webdoc.len(), "");

    Some(webdoc)
}

// https://cdn.esoui.com/downloads/file44/LibStub-1.0r6.zip?155847258615
fn extract_file_name(url: &str) -> Option<String> {
    let mut file_name = url.to_string();
    println!("URL: {}", url);

    let q_index = file_name.rfind('?');
    if q_index.is_some(){
        let q_index = q_index.unwrap();
        file_name.replace_range(q_index..file_name.len(), "");

        let first_slash_index = file_name.rfind('/').unwrap();
        file_name.replace_range(0..=first_slash_index, "");
    } else { return None; }

    Some(file_name)
}

fn download_zip(url: &str) -> Result<Vec<u8>> {
    let mut resp = reqwest::get(url).expect("Failed to get url from webdoc");
    let mut buf: Vec<u8> = vec![];
    resp.copy_to(&mut buf).expect("Failed to copy to buffer");
    Ok(buf)
}

fn prep_directory(dir: &PathBuf) -> Result<()> {
    if dir.exists() {
        Ok(())
    } else {
        create_directory(dir)
    }
}

fn create_directory(dir: &PathBuf) -> Result<()> {
    DirBuilder::new().recursive(true).create(dir)?;

    Ok(())
}
