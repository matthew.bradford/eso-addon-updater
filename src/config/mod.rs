use ron::de::from_reader;
use ron::ser::{to_string_pretty, PrettyConfig};

use std::io::Result;
use std::path::PathBuf;

use crate::file::{ open_file, write_file };

const FILE_NAME: &str = "eso_addon_config.ron";

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Config {
    eso_addon_directory: String,
    addon_urls: Vec<(String, String)>,
}

impl Config {
    pub fn get_eso_addon_dir(&self) -> Result<PathBuf> {
        Ok(PathBuf::from(&self.eso_addon_directory))
    }

    pub fn get_addon_urls(&self) -> &Vec<(String, String)> {
        &self.addon_urls
    }
}

pub fn fetch() -> Config {
    match open_file(FILE_NAME) {
        Some(t) => {
            let config: Config = match from_reader(t) {
                Ok(x) => {
                    println!("{:?}", x);
                    x
                }
                Err(e) => {
                    eprintln!("Error opening {}: {:?}", FILE_NAME, e);
                    new_config()
                }
            };
            config
        }
        None => new_config(),
    }
}

fn new_config() -> Config {
    println!("Creating new configuration file.");
    let config = Config::default();
    write_file(
        FILE_NAME,
        to_string_pretty(&config, PrettyConfig::default())
            .unwrap()
            .as_str()
            .as_bytes(),
    );
    config
}



/// Defaults based on machine
impl Default for Config {
    fn default() -> Config {
        Config {
            eso_addon_directory: String::new(),
            addon_urls: vec![(
                "addon_name".to_string(),
                "addon_url_use_click_here_link_on_download_page_remove?xxxx..".to_string(),
            )],
        }
    }
}
