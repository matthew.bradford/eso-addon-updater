use ron::de::from_reader;
use ron::ser::{to_string_pretty, PrettyConfig};

use std::collections::HashMap;
use std::io::Result;

use crate::file::{ open_file, write_file };

pub const VERSION_FILE: &str = "addons/version.ron";

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct VersionList {
    addons: HashMap<String, String>,
}

impl Default for VersionList {
    fn default() -> VersionList {
        VersionList {
            addons: HashMap::new(),
        }
    }
}

impl VersionList {
    pub fn update(&mut self, addon_name: &str, addon_version: &str) -> Result<()> {
        self.addons.insert(addon_name.to_string(), addon_version.to_string());
        Ok(())
    }

    pub fn outdated(&self, addon_name: &str, addon_version: &str) -> bool {
        let mut result = true;
        let target = self.addons.get(addon_name);
        if target.is_some() {
            let target = target.unwrap();
            if target == addon_version {
                result = false;
            } else {
                println!("Updating: {} to: {}", addon_name, addon_version);
            }
        }
        result
    }
}

fn new_version() -> VersionList {
    let version = VersionList::default();
    write_file(
        VERSION_FILE,
        to_string_pretty(&version, PrettyConfig::default())
            .unwrap()
            .as_str()
            .as_bytes(),
    );
    version
}

pub fn fetch() -> VersionList {
    match open_file(VERSION_FILE) {
        Some(t) => {
            let vlist: VersionList = match from_reader(t) {
                Ok(x) => {
                    println!("{:?}", x);
                    x
                }
                Err(e) => {
                    eprintln!("Error opening {}: {:?}", VERSION_FILE, e);
                    new_version()
                }
            };
            vlist
        }
        None => new_version(),
    }
}

pub fn save(list: &VersionList) -> Result<()> {
    write_file(
        VERSION_FILE,
        to_string_pretty(list, PrettyConfig::default())
            .unwrap()
            .as_str()
            .as_bytes(),
    );
    Ok(())
}